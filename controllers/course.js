const Course =require ("../model/Course")
const courseController = require("../controllers/course");
const userController = require("../controllers/user");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// creae a new course
// the addCourse is get in  route/course.js
// use async para mareturn yung promise
module.exports.addCourse = async (user,reqBody, res)=>{
	if(user.isAdmin){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((course,error)=>{
		if (error) {
			return false;
		}else{
			
			return	true;
		}

	})

	}
	else{
		return("you have no acess")
	}
}

// Controller method for retrieving aall the courses
module.exports	.getAllCourses=() =>{
 return	Course.find({}).then(result =>{
 	return result;
 })
 }


 // Retrieve all active course
 module.exports.getAllActive =() =>{
 	return Course.find({isActive : true}).then(result =>{
 		return result;
 	})
 }


 // Retrieving a secific course
 module.exports.getCourse = (reqParams)=>{
 	return Course.findById(reqParams.courseId).then(result =>{
 		return result;
 	})
 }

 // update a course
 module.exports.updateCourse = async (user,reqParams,reqBody)=>{
	 	if(user.isAdmin){
	 		 	// specify the fileds/properties of the document to be updated
	 	let updatedCourse ={
	 		name :reqBody.name,
	 		description: reqBody.description,
	 		price: reqBody.price
	 	}
	 	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then ((course,error)=>{
	 		if(error){
	 			return false;
	 		}
	 		else{
	 			return true;
	 		}
	 	})
 	}
 	else{
 			return("you have no access")
 	}


 }

 // activity
 // archived a course
 module.exports.archivedCourse = async (user,reqParams,reqBody)=>{
	 	if(user.isAdmin){
	 		 	// specify the fileds/properties of the document to be updated
	 	let archivedCourse ={
	 		isActive :reqBody.isActive
	 		
	 	}
	 	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then ((course,error)=>{
	 		if(error){
	 			return false;
	 		}
	 		else{
	 			return true;
	 		}
	 	})
 	}
 	else{
 			return("you have no access")
 	}


 }