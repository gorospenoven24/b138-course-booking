const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Route retrieving user details
// get method for retrieving details
// auth verify method acts as a middleware to ensure the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	// provides the users ID for the getprofile controller method
	userController.getProfile({userId :userData.id}).then(resultFromController => res.send(resultFromController));
});



// Route to enroll a user to a course
router.post("/enroll",auth.verify,(req,res)=>{
	
	const userData = auth.decode(req.headers.authorization);
	userController.enroll(userData, req.body).then(resultFromController => res.send(resultFromController));

})


// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;
