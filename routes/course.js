const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// route for creating a course
// router.post("/",(req,res)=>{
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })

router.post("/", auth.verify, (req, res) => {
			const data = auth.decode(req.headers.authorization)
			courseController.addCourse(data, req.body).then(resultFromController => res.send(resultFromController));

		});


// Route for retrieving al the courses
router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// route  for retrieving all active courses
router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController	));
})

// route for retriver a specific csuorse
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController =>res.send(resultFromController));
})



//  route for updating course
router.put("/:courseId", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	courseController.updateCourse(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


//  route for archiving course
router.put("/:courseId/archive", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	courseController.archivedCourse(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// allow us to export the "router" that will acces the index.js
module.exports = router